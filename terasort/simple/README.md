Terasort
=============

Terasort is a popular algorithm for sorting large files in parallel. In this example, we show Terasort algorithm applied to uniformly distributed real numbers in <0,1> interval implemented using GASPI.

Basic scheme
----------

The central idea of the algorithm is to distribute the sorting workload among multiple nodes via partitioning of the large input file using a 
`hash function`. Partitions are collected from all ranks, sorted locally, and finally written to an output file.
The role of the hash function is to assign destination partition for each sorted element, and its form is problem-specific, in general. 
In this case, we sort uniformly distributed real numbers in interval <0,1>, thus the hash function assigns rank to value to uniformly 
distribute data among all ranks (see schema below).


Visualization of the Terasort algorithm. Example of sorting 10 numbers (0-9) store in a file by 2 ranks.
----------

      1. Each rank read it's file part using MPI-IO 

        unsorted file                      rank 0            rank 1
        ___________________      MPI-IO    _________         _________
       |3 7 1 9 0 8 2 5 4 6|     read     |3 7 1 9 0|       |8 2 5 4 6|
       |___________________|     ----->   |_________|       |_________|   


     2. Partition data using the hash function locally and distribute / exchange the data among ranks using GASPI

        rank 0                          rank 0                                     rank 0             
        _________                       _____         ___                          _____         ___  
       |3 7 1 9 0|     hash function   |3 1 0|       |7 9|                        |3 1 0|       |2 4|  
       |_________|     ------------>   |_____|       |___|                        |_____|       |___|  
                                                                 shuffle data
        rank 1                          rank 1                   ------------>     rank 1              
        _________                       ___           _____                        ___           _____                
       |8 2 5 4 6|     hash function   |2 4|         |8 5 6|                      |7 9|         |8 5 6|
       |_________|     ------------>   |___|         |_____|                      |___|         |_____|


     3. Sort data locally and write the file chunks to a single output file using MPI-IO 

	rank 0                          rank 0                                               
        _________                       _________                          
       |3 1 0 2 4|        qsort        |0 1 2 3 4|                         
       |_________|        ----->       |_________|               MPI-IO       sorted file 
                                                                 write        ___________________   
        rank 1                          rank 1                   ------>     |0 1 2 3 4 5 6 7 8 9|              
        _________                       _________                            |___________________|       
       |7 9 8 5 6|        qsort        |5 6 7 8 9|                                     
       |_________|        ----->       |_________|                                        
                                

Code section
----------

The essential part of our approach is the use of GASPI segments for asynchronous communication and data transfer among ranks.
Here is an example of a GASPI segment creation, for storing element counts on each rank:

```
    gaspi_segment_id_t seg_id_n_parts_out = 0;
    
    seg_length = sizeof (long int);
    gaspi_segment_create ( seg_id_n_parts_out, size*seg_length, GASPI_GROUP_ALL,GASPI_BLOCK, GASPI_MEM_INITIALIZED );
    gaspi_segment_ptr ( seg_id_n_parts_out, & seg_ptr );
    n_parts = (long int *) seg_ptr;
```

Terasort algorithm (without buffering)
----------

First step of the Terasort algorithm is to read elements from (binary, in this case) file to local array `data` using MPI_IO parallel collective procedure `MPI_File_read_at_all`:
    

 ```
    MPI_File_read_at_all( f, offset,  data,  n_part,  MPI_DOUBLE,  &status );
```


In the next step, number of elements (from local data segments) that will be transfered to other ranks is counted. Hash function is used to assign the destinations rank and the sorted element is copied  to an 2-dimensional local array 

```
    for(i=0; i<n_part; i++)
    {
        j = (int)trunc(data[i]*size);  // Hash function attaches right rank (j) for the sorted element: data[i]
        splited_parts[j][n_parts[j]] = data[i]; // Copy the sorted element to a 2-dimensional local array
        n_parts[j]++; // Increase number of elements for rank j
    }
```

After the local data is partitioned, elements from the 2-dimensional array are copied to the 1-dimensional GASPI segment:

```
    k = 0;
    for(i=0; i<size; i++)
    {
        for(j=0; j<n_parts[i]; j++)
        {
            parts_out[k] = splited_parts[i][j];
            k++;
        }
    }
```

The outgoing element counts are exchanged among ranks prior to the data transfer, using the GASPI write procedure `gaspi_write_notify`:

```
    for(i=0; i<size; i++)
    {
        if( i != rank )
        {
            offset_local = i*(sizeof(long int));      // Local offset
            offset_remote = rank*(sizeof(long int));  // Remote offset

            while ( gaspi_write_notify ( seg_id_n_parts_out, offset_local, i, seg_id_n_parts_loc, offset_remote, sizeof(long int), not_id, not_val, queue_id, GASPI_BLOCK ) == GASPI_QUEUE_FULL )
            {
                gaspi_wait( queue_id, GASPI_BLOCK);   // If the notification queue is full, queue is emptied by calling a blocking gaspi_wait procedure
            }
        }
    }
```

Completion of GASPI write calls is confirmed by the GASPI notification check procedure `gaspi_notify_waitsome`:

 ```
    for(i=0; i<size-1; i++) // Wait for notifications from all ranks
    {
        not_val = 0;
        gaspi_notify_waitsome (seg_id_n_parts_loc, 0, size, &first_id, GASPI_BLOCK );  // Waiting for notification from a rank (in arbitrary order)
        gaspi_notify_reset (seg_id_n_parts_loc, first_id, &not_val);    // Incoming notification is resetted
    }
```

In next step, each rank count how many elements come from all ranks and create GASPI segment for store these elements. 

Then each rank transfer to all ranks number of elements to be sorted on this rank.

Finally, each rank transfer partitioned local data to all ranks by GASPI write procedure:



Next, each rank collects the element counts from all other ranks, creates a GASPI segment of the appropriate size, and sends this information to other ranks. Finally, partitioned local data is transferred to all ranks using the GASPI write procedure:

 ```
    for(i=0; i<size; i++)
    {
        if( i != rank )
        {
            sum_n_parts = 0; 
            for(j=0; j<i; j++)
               sum_n_parts = sum_n_parts + n_parts[j];     // Count offset in partitioned local data segment

            offset_local = sum_n_parts*(sizeof(double));

            sum_n_parts_rem = 0;
            for(j=0; j<rank; j++)                   // Count offset in remote data segment for store elements
                sum_n_parts_rem = sum_n_parts_rem + n_parts_rem[i*size+j];  
            offset_remote = sum_n_parts_rem*(sizeof(double));

            while ( gaspi_write_notify ( seg_id_parts_out, offset_local, i, seg_id_parts_in, offset_remote, n_parts[i]*sizeof(double), not_id, not_val, queue_id, GASPI_BLOCK ) == GASPI_QUEUE_FULL )
            {
                gaspi_wait( queue_id, GASPI_BLOCK);
            }
        }
    }
```

After the data is successfully transfer to all ranks, local elements are sorted using standard quicksort algorithm `qsort`:
    
 ```
    qsort(parts_in, sum_n_parts_loc, sizeof(double), compare);
```

Last step of the algorithm is to write the sorted elements to binary file using the MPI_IO parallel collective procedure `MPI_File_write_at_all`:

 ```
    MPI_File_write_at_all( f, offset,  parts_in,  sum_n_parts_loc,  MPI_DOUBLE,  &status );
```









