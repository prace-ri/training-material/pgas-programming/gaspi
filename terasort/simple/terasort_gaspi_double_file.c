#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <mpi.h>
#include <GASPI.h>

int compare (const void * a, const void * b)
{
    if ( *(double*)a <  *(double*)b )
        return -1;
    else
        return  1;
}

int main(int argc, char *argv[])
{
    // MPI initialization
    int rank, size;
    MPI_Status status;
    MPI_Request request;
    MPI_Comm comm;
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);

    // GASPI initialization
    gaspi_proc_init(GASPI_BLOCK);

    // GASPI variables
    gaspi_notification_id_t not_id = rank, first_id;
    gaspi_notification_t not_val = 1, not_res = 0;
    gaspi_segment_id_t seg_id_n_parts_out = 0, seg_id_parts_out = 1, seg_id_parts_in = 2, seg_id_n_parts_rem = 3, seg_id_n_parts_loc = 4;
    gaspi_queue_id_t queue_id = 0;
    gaspi_pointer_t seg_ptr;
    gaspi_size_t seg_length;
    gaspi_offset_t offset_local = 0, offset_remote = 0;

    struct timeval start, end;

    long int i,j,k;
    long int n, n_part;
    double *data;
    MPI_File f;

    //
    // Read unsorted real numbers from binary file using MPI_IO parallel collective procedure
    //

    if ( rank == 0 )
        gettimeofday(&start, NULL);

    // Read name of the input file
    if (argc > 1 )
        MPI_File_open(MPI_COMM_WORLD, argv[1],  MPI_MODE_RDONLY,  MPI_INFO_NULL, &f);

    // Default name of the input file
    else
        MPI_File_open(MPI_COMM_WORLD, "unsorted.dat",  MPI_MODE_RDONLY,  MPI_INFO_NULL, &f);

    MPI_Offset offset = 0;

    // Get the file size
    MPI_File_get_size( f, &offset);

    // Calculate total number of elements in the file
    n = offset/sizeof(double);

    // Number of elements on each rank
    n_part = offset/size/sizeof(double);

    data = (double*)malloc(n_part*sizeof(double));

    offset = rank*n_part*sizeof(double);

    // Read the file in parallel (simultaneously on each rank)
    MPI_File_read_at_all( f, offset,  data,  n_part,  MPI_DOUBLE,  &status );

    MPI_File_close(&f);

    if( rank == 0 )
    {
        gettimeofday(&end, NULL);
        printf(" Number of elements in file: %Ld \n", n);
        printf(" Number of ranks: %d \n", size);
        printf(" Read file time: ");
        printf(" %lf sec\n",end.tv_sec-start.tv_sec + (end.tv_usec-start.tv_usec)/1000000. );
    }

    //
    // Terasort
    //

    if ( rank == 0 )
        gettimeofday(&start, NULL);

    double **splited_parts;
    long int *n_parts, sum_n_parts, sum_n_parts_loc, sum_n_parts_rem;

    // Create GASPI segment for storing element counts on each rank
    seg_length = sizeof (long int);
    gaspi_segment_create ( seg_id_n_parts_out, size*seg_length, GASPI_GROUP_ALL,GASPI_BLOCK, GASPI_MEM_INITIALIZED );
    gaspi_segment_ptr ( seg_id_n_parts_out, & seg_ptr );
    n_parts = (long int *) seg_ptr;

    //
    // Split elements to ranks
    //

    // Array of element counts on each rank
    for(i=0; i<size; i++)
    {
        n_parts[i] = 0;
    }

    // Count elements on each rank
    for(i=0; i<n_part; i++)
    {
        j = (int)trunc(data[i]*size);  // Attach the right rank to each element
        n_parts[j]++;
    }

    // Arrays to store split elements
    splited_parts = (double **)malloc(size*sizeof(double *));

    for(i=0; i<size; i++)
    {
        splited_parts[i] = (double *)malloc(n_parts[i]*sizeof(double));
    }

    // Clear count array
    for(i=0; i<size; i++)
    {
        n_parts[i] = 0;
    }

    // Split elements to right ranks
    for(i=0; i<n_part; i++)
    {
        j = (int)trunc(data[i]*size);  // Attach right rank to each element
        splited_parts[j][n_parts[j]] = data[i];
        n_parts[j]++;
    }

    // Delete the origin data array
    free(data);

    double *parts_out;

    // Create GASPI segment for storing split elements on each rank
    seg_length = sizeof (double);
    gaspi_segment_create ( seg_id_parts_out, n_part*seg_length, GASPI_GROUP_ALL,GASPI_BLOCK, GASPI_MEM_INITIALIZED );
    gaspi_segment_ptr ( seg_id_parts_out, & seg_ptr );
    parts_out = (double *) seg_ptr ;

    // Transfer 2-dimension array of split elements to 1-dimension GASPI segment
    k = 0;
    for(i=0; i<size; i++)
    {
        for(j=0; j<n_parts[i]; j++)
        {
            parts_out[k] = splited_parts[i][j];
            k++;
        }
    }

    if( rank == 0 )
    {
        gettimeofday(&end, NULL);
        printf(" Splitting of elements: ");
        printf(" %lf sec\n",end.tv_sec-start.tv_sec + (end.tv_usec-start.tv_usec)/1000000. );
    }

    // Delete the 2-dimension array of split elements
    for(i=0; i<size; i++)
    {
        free(splited_parts[i]);
    }

    free(splited_parts);

    //
    // GASPI data sharing of split elements among ranks
    //

    if ( rank == 0 )
        gettimeofday(&start, NULL);

    double *parts_in;
    long int *n_parts_loc, *n_parts_rem;

    // Create GASPI segment for incoming split elements count from each rank
    seg_length = sizeof (long int);
    gaspi_segment_create ( seg_id_n_parts_loc, size*seg_length, GASPI_GROUP_ALL,GASPI_BLOCK, GASPI_MEM_INITIALIZED );
    gaspi_segment_ptr ( seg_id_n_parts_loc, & seg_ptr );
    n_parts_loc = (long int *) seg_ptr ;

    offset_local = 0;
    offset_remote = 0;

    // Send elements count to each rank using GASPI write
    for(i=0; i<size; i++)
    {
        if( i != rank )
        {
            offset_local = i*(sizeof(long int));
            offset_remote = rank*(sizeof(long int));

            while ( gaspi_write_notify ( seg_id_n_parts_out, offset_local, i, seg_id_n_parts_loc, offset_remote, sizeof(long int), not_id, not_val, queue_id, GASPI_BLOCK ) == GASPI_QUEUE_FULL )
            {
                gaspi_wait( queue_id, GASPI_BLOCK);
            }
        }
    }

    // Wait for completion of GASPI write calls
    for(i=0; i<size-1; i++)
    {
        not_val = 0;
        gaspi_notify_waitsome (seg_id_n_parts_loc, 0, size, &first_id, GASPI_BLOCK );
        gaspi_notify_reset (seg_id_n_parts_loc, first_id, &not_val);
    }

    gaspi_wait(queue_id, GASPI_BLOCK);

    n_parts_loc[rank] = n_parts[rank];

    // Count number of all elements attributable to this rank
    sum_n_parts_loc = 0;
    for(i=0; i<size; i++)
    {
        sum_n_parts_loc = sum_n_parts_loc + n_parts_loc[i];
    }

    // Create GASPI segment for incoming split elements from each rank
    seg_length = sizeof (double);
    gaspi_segment_create ( seg_id_parts_in, sum_n_parts_loc*seg_length, GASPI_GROUP_ALL,GASPI_BLOCK, GASPI_MEM_INITIALIZED );
    gaspi_segment_ptr ( seg_id_parts_in, & seg_ptr );
    parts_in = (double *) seg_ptr ;

    // Create GASPI segment to store split element counts for each rank
    seg_length = sizeof (long int);
    gaspi_segment_create ( seg_id_n_parts_rem, size*size*seg_length, GASPI_GROUP_ALL,GASPI_BLOCK, GASPI_MEM_INITIALIZED );
    gaspi_segment_ptr ( seg_id_n_parts_rem, & seg_ptr );
    n_parts_rem = (long int *) seg_ptr ;

    // Send elements count attributable to this rank to each other rank
    for(i=0; i<size; i++)
    {
        if( i != rank )
        {
            offset_local = 0;
            offset_remote = rank*size*(sizeof(long int));

            while ( gaspi_write_notify ( seg_id_n_parts_loc, offset_local, i, seg_id_n_parts_rem, offset_remote, size*sizeof(long int), not_id, not_val, queue_id, GASPI_BLOCK ) == GASPI_QUEUE_FULL )
            {
                gaspi_wait( queue_id, GASPI_BLOCK);
            }
        }
    }

    // Wait for completion of GASPI write calls
    for(i=0; i<size-1; i++)
    {
        not_val = 0;
        gaspi_notify_waitsome (seg_id_n_parts_rem, 0, size, &first_id, GASPI_BLOCK );
        gaspi_notify_reset (seg_id_n_parts_rem, first_id, &not_val);
    }


    // Send elements to other ranks
    for(i=0; i<size; i++)
    {
        if( i != rank )
        {
            sum_n_parts = 0;
            for(j=0; j<i; j++)
                sum_n_parts = sum_n_parts + n_parts[j];
            offset_local = sum_n_parts*(sizeof(double));

            sum_n_parts_rem = 0;
            for(j=0; j<rank; j++)
                sum_n_parts_rem = sum_n_parts_rem + n_parts_rem[i*size+j];
            offset_remote = sum_n_parts_rem*(sizeof(double));

            while ( gaspi_write_notify ( seg_id_parts_out, offset_local, i, seg_id_parts_in, offset_remote, n_parts[i]*sizeof(double), not_id, not_val, queue_id, GASPI_BLOCK ) == GASPI_QUEUE_FULL )
            {
                gaspi_wait( queue_id, GASPI_BLOCK);
            }
        }
    }

    // Wait for completion of GASPI write calls
    for(i=0; i<size-1; i++)
    {
        not_val = 0;
        gaspi_notify_waitsome (seg_id_parts_in, 0, size, &first_id, GASPI_BLOCK );
        gaspi_notify_reset (seg_id_parts_in, first_id, &not_val);
    }

    // Copy the rest of (i.e. "local") attributable elements to local array
    sum_n_parts = 0;
    for(j=0; j<rank; j++)
        sum_n_parts = sum_n_parts + n_parts[j];

    sum_n_parts_loc = 0;
    for(j=0; j<rank; j++)
        sum_n_parts_loc = sum_n_parts_loc + n_parts_loc[j];

    for(i=0; i<n_parts[rank]; i++)
    {
        parts_in[sum_n_parts_loc+i] = parts_out[sum_n_parts+i];
    }

    gaspi_wait( queue_id, GASPI_BLOCK);

    // Delete split elements
    gaspi_segment_delete( seg_id_parts_out );

    // Count local elements count for sorting
    sum_n_parts_loc = 0;
    for(j=0; j<size; j++)
        sum_n_parts_loc = sum_n_parts_loc + n_parts_loc[j];

    if( rank == 0 )
    {
        gettimeofday(&end, NULL);
        printf(" Data sharing: ");
        printf(" %lf sec\n",end.tv_sec-start.tv_sec + (end.tv_usec-start.tv_usec)/1000000. );
    }

    // Local data sorting
    if ( rank == 0 )
        gettimeofday(&start, NULL);

    // Sort of elements on each rank
    qsort(parts_in, sum_n_parts_loc, sizeof(double), compare);

    MPI_Barrier(MPI_COMM_WORLD);

    if( rank == 0 )
    {
        gettimeofday(&end, NULL);
        printf(" Data sorting time: ");
        printf(" %lf sec\n",end.tv_sec-start.tv_sec + (end.tv_usec-start.tv_usec)/1000000. );
    }

    if ( rank == 0 )
    {
        gettimeofday(&start, NULL);
    }

    //
    // Write locally sorted elements to binary file using MPI_IO parallel collective procedure
    //

    MPI_File_delete("sorted.dat", MPI_INFO_NULL);

    MPI_File_open (MPI_COMM_WORLD, "sorted.dat",  MPI_MODE_CREATE|MPI_MODE_WRONLY,  MPI_INFO_NULL, &f);

    // Calculate offset for parallel write
    sum_n_parts_rem = 0;
    for(j=0; j<size*rank; j++)
        sum_n_parts_rem = sum_n_parts_rem + n_parts_rem[j];

    offset = sum_n_parts_rem*sizeof(double);

    MPI_File_write_at_all( f, offset,  parts_in,  sum_n_parts_loc,  MPI_DOUBLE,  &status );

    MPI_File_close(&f);

    if( rank == 0 )
    {
        gettimeofday(&end, NULL);
        printf(" Write data time: ");
        printf(" %lf sec \n\n",end.tv_sec-start.tv_sec + (end.tv_usec-start.tv_usec)/1000000. );
    }

    // GASPI finalizations
    gaspi_wait ( queue_id, GASPI_BLOCK );
    gaspi_barrier ( GASPI_GROUP_ALL, GASPI_BLOCK );
    gaspi_proc_term ( GASPI_BLOCK );

    // MPI finalizations
    MPI_Finalize();
    return 0;
}
