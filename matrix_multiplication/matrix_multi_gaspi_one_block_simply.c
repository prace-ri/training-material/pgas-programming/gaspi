#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <mkl.h>
#include <GASPI.h>

#define min(x,y) (((x) < (y)) ? (x) : (y))

int main(int argc, char *argv[])
{
    // GASPI initialization
    gaspi_proc_init(GASPI_BLOCK);

    // GASPI variables
    gaspi_rank_t rank, size;
    gaspi_proc_rank(&rank);
    gaspi_proc_num(&size);
    gaspi_queue_id_t queue_id = 0;
    gaspi_pointer_t seg_ptr;
    gaspi_size_t seg_length;
    gaspi_offset_t offset_local = 0, offset_remote = 0;
    gaspi_notification_id_t not_id = rank, first_id;
    gaspi_notification_t not_val = 1, not_res = 0;

    double *A, *B, *C;
    long int m, n, k, i, j, l;
    double alpha, beta;

    struct timeval start, end;

    // Read and validate input parameters
    if (argc > 1 )
        m = atol(argv[1]); // Read size of matrix
    else
        m = 1000; // Default size of matrix

    // Square matrices only
    k = m, n = m;

    long int block_size, p_row, p_colum, p_size;

    // Size of (square only) process grid
    p_size = (int)(sqrt(size));

    // Size of matrix block for each rank
    block_size = m/p_size;

    // Indices of rank in the process grid
    p_row = rank/p_size;
    p_colum = rank%p_size;

    if ( rank == 0 )
        printf( "\n Matrix size: %d  Block size: %d  Process size: = %d\n", n, block_size, p_size );

    if ( rank == 0 )
        gettimeofday(&start, NULL);

    // Create GASPI segments for storing blocks of matrices A,B,C
    gaspi_segment_id_t seg_id_A = 0, seg_id_B = 1, seg_id_C = 2, seg_id_read_buffer_A = 3, seg_id_read_buffer_B = 4;

    seg_length = sizeof(double);
    gaspi_segment_create ( seg_id_A, block_size*block_size*seg_length, GASPI_GROUP_ALL,GASPI_BLOCK, GASPI_MEM_INITIALIZED );
    gaspi_segment_ptr ( seg_id_A, &seg_ptr );
    A = (double *) seg_ptr;

    seg_length = sizeof(double);
    gaspi_segment_create ( seg_id_B, block_size*block_size*seg_length, GASPI_GROUP_ALL,GASPI_BLOCK, GASPI_MEM_INITIALIZED );
    gaspi_segment_ptr ( seg_id_B, &seg_ptr );
    B = (double *) seg_ptr;

    seg_length = sizeof(double);
    gaspi_segment_create ( seg_id_C, block_size*block_size*seg_length, GASPI_GROUP_ALL,GASPI_BLOCK, GASPI_MEM_INITIALIZED );
    gaspi_segment_ptr ( seg_id_C, &seg_ptr );
    C = (double *) seg_ptr;

    // Create GASPI segments for matrix A and B block buffers
    double *read_buffer_A, *read_buffer_B;
    int switcher = 0;

    seg_length = sizeof(double);
    gaspi_segment_create( seg_id_read_buffer_A, 2*block_size*block_size*seg_length, GASPI_GROUP_ALL,GASPI_BLOCK, GASPI_MEM_INITIALIZED );
    gaspi_segment_ptr( seg_id_read_buffer_A, & seg_ptr );
    read_buffer_A = (double *) seg_ptr;

    seg_length = sizeof(double);
    gaspi_segment_create( seg_id_read_buffer_B, 2*block_size*block_size*seg_length, GASPI_GROUP_ALL,GASPI_BLOCK, GASPI_MEM_INITIALIZED );
    gaspi_segment_ptr( seg_id_read_buffer_B, & seg_ptr );
    read_buffer_B = (double *) seg_ptr;


    // Create some initial content of matrices A and B
    long int row, column;

    // Calculate offsets of matrix A and B blocks
    row = p_row*p_size*block_size*block_size;
    column = p_colum*block_size;

    // Fill with real numbers in interval <0,1>
    for (i = 0; i < block_size; i++)
        for (j = 0; j < block_size; j++)
        {
            A[i*block_size+j] = (double)(row+column+i*p_size*block_size+j+1)/(m*k);
        }

    for (i = 0; i < block_size; i++)
        for (j = 0; j < block_size; j++)
        {
            B[i*block_size+j] = (double)(-1*(row+column+i*p_size*block_size+j)-1)/(m*k);
        }

    // Synchronize all ranks here
    gaspi_barrier ( GASPI_GROUP_ALL, GASPI_BLOCK );

    // First, read blocks of matrices A and B before (block) matrix multiplication
    if ( rank == 0 )
        gettimeofday(&start, NULL);

    int ii, rank_A, rank_B, rank_order = 0;

    // Initialize active /inactive switch of GASPI segment input blocks
    switcher = 1;

    // Calculate initial offset of A_k * B_k block pairs in the multiply chain
    // for better (initial) work distribution.
    if( p_row == p_colum )
    {
        rank_order = p_row;
    }
    else
    {
        rank_order = p_row + p_colum;
        if( rank_order > p_size-1 )
            rank_order = rank_order - p_size;
    }

    // Find source ranks for the initial read
    rank_A = p_size*p_row + rank_order;
    rank_B = p_size*rank_order + p_colum;

    offset_local = switcher*block_size*block_size*sizeof(double);
    offset_remote = 0;

    // Initial read of A and B blocks. If block(s) of matrices A and/or B are stored locally (i.e. on the same rank)
    // memcpy function is used instead (no data transfer).
    if( rank_A == rank && rank_B == rank )
    {
        memcpy( &read_buffer_A[switcher*block_size*block_size], &A[0], block_size*block_size*sizeof(double));
        memcpy( &read_buffer_B[switcher*block_size*block_size], &B[0], block_size*block_size*sizeof(double));
    }

    if( rank_A == rank && rank_B != rank )
    {
        gaspi_read_notify( seg_id_read_buffer_B, offset_local, rank_B, seg_id_B, offset_remote, block_size*block_size*sizeof(double), not_id, queue_id, GASPI_BLOCK);

        memcpy( &read_buffer_A[switcher*block_size*block_size], &A[0], block_size*block_size*sizeof(double));

        gaspi_notify_waitsome( seg_id_read_buffer_B, 0, size, &first_id, GASPI_BLOCK );
        gaspi_notify_reset(seg_id_read_buffer_B, first_id, &not_res);
    }

    if( rank_A != rank && rank_B == rank )
    {
        gaspi_read_notify( seg_id_read_buffer_A, offset_local, rank_A, seg_id_A, offset_remote, block_size*block_size*sizeof(double), not_id, queue_id, GASPI_BLOCK);

        memcpy( &read_buffer_B[switcher*block_size*block_size], &B[0], block_size*block_size*sizeof(double));

        gaspi_notify_waitsome( seg_id_read_buffer_A, 0, size, &first_id, GASPI_BLOCK );
        gaspi_notify_reset(seg_id_read_buffer_A, first_id, &not_res);
    }

    if( rank_A != rank && rank_B != rank )
    {
        gaspi_read_notify( seg_id_read_buffer_A, offset_local, rank_A, seg_id_A, offset_remote, block_size*block_size*sizeof(double), not_id, queue_id, GASPI_BLOCK);
        gaspi_read_notify( seg_id_read_buffer_B, offset_local, rank_B, seg_id_B, offset_remote, block_size*block_size*sizeof(double), not_id, queue_id, GASPI_BLOCK);

        gaspi_notify_waitsome( seg_id_read_buffer_A, 0, size, &first_id, GASPI_BLOCK );
        gaspi_notify_reset(seg_id_read_buffer_A, first_id, &not_res);

        gaspi_notify_waitsome( seg_id_read_buffer_B, 0, size, &first_id, GASPI_BLOCK );
        gaspi_notify_reset(seg_id_read_buffer_B, first_id, &not_res);
    }

    //
    // Main matrix multiplication scheme
    //

    // Dgemm parameters
    alpha = 1.0;
    beta = 1.0;

    // Exchange active and inactive input blocks
    switcher = (switcher+1)%2;

    for (ii=1; ii < p_size; ii++)
    {
        rank_order++;

        if( rank_order == p_size )
        {
            rank_order = 0;
        }

        // Find ranks order for the next read of blocks of matrix A and B
        rank_A = p_size*p_row + rank_order;
        rank_B = p_size*rank_order + p_colum;

        offset_local = switcher*block_size*block_size*sizeof(double);
        offset_remote = 0;

        // Read blocks of matrix A and B for the next multiplication
        gaspi_read_notify( seg_id_read_buffer_A, offset_local, rank_A, seg_id_A, offset_remote, block_size*block_size*sizeof(double), not_id, queue_id, GASPI_BLOCK);
        gaspi_read_notify( seg_id_read_buffer_B, offset_local, rank_B, seg_id_B, offset_remote, block_size*block_size*sizeof(double), not_id, queue_id, GASPI_BLOCK);

        // Exchange active and inactive input blocks
        switcher = (switcher+1)%2;

        // dgemm block matrix multiplication
        cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, block_size, block_size, block_size, alpha, &read_buffer_A[switcher*block_size*block_size], block_size, &read_buffer_B[switcher*block_size*block_size], block_size, beta, C, block_size);

        // Wait for completion of blocks of matrix A and B reads
        gaspi_notify_waitsome( seg_id_read_buffer_A, 0, size, &first_id, GASPI_BLOCK );
        gaspi_notify_reset(seg_id_read_buffer_A, first_id, &not_res);

        gaspi_notify_waitsome( seg_id_read_buffer_B, 0, size, &first_id, GASPI_BLOCK );
        gaspi_notify_reset(seg_id_read_buffer_B, first_id, &not_res);
    }

    switcher = (switcher+1)%2;

    // Last dgemm block matrix multiplication
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, block_size, block_size, block_size, alpha, &read_buffer_A[switcher*block_size*block_size], block_size, &read_buffer_B[switcher*block_size*block_size], block_size, beta, C, block_size);

    // Synchronize all ranks here
    gaspi_barrier ( GASPI_GROUP_ALL, GASPI_BLOCK );

    // Print some statistics
    if ( rank == 0 )
    {
        gettimeofday(&end, NULL);

        printf("\n Multiplication time: ");
        printf(" %lf sec\n", end.tv_sec-start.tv_sec + (end.tv_usec-start.tv_usec)/1000000. );
    }

    // Print top left corner (size of max. 6x6) of blocks of matrices A, B and C on rank 0
    if ( rank == 0 )
    {
        printf ("\n Top left corner of matrix A: \n");
        for (i=0; i<min(m,6); i++)
        {
            for (j=0; j<min(n,6); j++)
            {
                printf (" %lf", A[j+i*block_size]);
            }
            printf ("\n");
        }
        printf ("\n Top left corner of matrix B: \n");
        for (i=0; i<min(m,6); i++)
        {
            for (j=0; j<min(n,6); j++)
            {
                printf (" %lf", B[j+i*block_size]);
            }
            printf ("\n");
        }
        printf ("\n Top left corner of matrix C: \n");
        for (i=0; i<min(m,6); i++)
        {
            for (j=0; j<min(n,6); j++)
            {
                printf (" %lf", C[j+i*block_size]);
            }
            printf ("\n");
        }
    }

    // GASPI finalization
    gaspi_wait( queue_id, GASPI_BLOCK );
    gaspi_barrier( GASPI_GROUP_ALL, GASPI_BLOCK );
    gaspi_proc_term( GASPI_BLOCK );

    return 0;
}
