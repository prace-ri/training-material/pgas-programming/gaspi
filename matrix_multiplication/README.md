Parallel Matrix Multiplication using GASPI
=============

In this example we show an implementation of distributed parallel matrix
multiplication (C = A * B) for square matrices using GASPI. This algorithm
utilizes asynchronous, one-sided, RDMA driven communication in PGAS, as provided
by GPI-2 library, an implementation of the GASPI standard.

Basic scheme
----------

The goal of this approach is to maximize the overlap of communication and
computation via "double buffered" approach. To map blocks of matrices on
available parallel processes (ranks) we use a concept of (squered, for
simplicity) processing grid.

Visualization of matrix A distribution accross ranks (square matrix size of 4x4, process grid 2x2)
----------

        matrix A                           rank 0            rank 1
        ___________                        _____             _____
       |1  2 |3  4 |                      |1  2 |           |3  4 |
       |5  6 |7  8 |                      |5  6 |           |7  8 |
       |_____|_____|         --->         |_____|           |_____|
       |9  10|11 12|                      
       |13 14|15 16|                       rank 2            rank 3
       |_____|_____|                       _____             _____
                                          |9  10|           |11 12|
                                          |13 14|           |15 16|
                                          |_____|           |_____|

The algorithm itself is can be described as following: each rank has two sets of
GASPI segments: first, or the `output` segment serves for persistent storage of
block(s) of matrices A and B, that other ranks can read without active
participation of the host rank (asynchronous one-sided RDMA communication).
Second, it is the `input` segment, which (temporarily) stores matrix blocks
read from remote ranks, in parallel to the (block) matrix multiplication.
Within the `input` block of GASPI segments, one pair of A and B blocks is
active and one is inactive at a time. `Active` means that blocks contain all
data and participates in local DGEMM matrix multiplication, while `inactive`
labels the destination memory segment for asynchronous reads from remote ranks.
Once the local DGEMM computation and the communication with remote ranks is
finished, active and inactive segments switch their roles and the resulting C
matrix block (stored in the local memory) is updated. That way the
communication and the computation do not block each other.

Essential part of the approach is in use of GASPI segments for asynchronous
data transfer between ranks. Here is example of creation of a segment for
storing matrix A block

```
    gaspi_segment_id_t seg_id_A = 0;

    seg_length = sizeof(double);
    gaspi_segment_create ( seg_id_A, block_size*block_size*seg_length, GASPI_GROUP_ALL,GASPI_BLOCK, GASPI_MEM_INITIALIZED );
    gaspi_segment_ptr ( seg_id_A, &seg_ptr );
    A = (double *) seg_ptr;
```

We use GASPI `read` function for the data transfer. Data is transferred on the
background, i.e.  after calling read, rank can immediately do computation while
waiting for notification from remote rank:

 ```
    // (read) Read blocks of matrix A for the next multiplication
    read_notify_and_cycle(seg_id_read_buffer_A, l_p, rank_A, seg_id_A, r_p, block_size*block_size*sizeof(double), not_id );
    
    // (compute) dgemm block matrix multiplication
    cblas_dgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, block_size, block_size, block_size, alpha, &read_buffer_A[switcher*block_size*block_size], block_size, &read_buffer_B[switcher*block_size*block_size], block_size, beta, C, block_size);

    // (wait for notification) Wait for completion of block of matrix A read
    gaspi_notify_waitsome( seg_id_read_buffer_A, 0, size, &first_id, GASPI_BLOCK );
    gaspi_notify_reset(seg_id_read_buffer_A, first_id, &not_res);
```

Requirements
------------

Please visit GPI-2 home page ( http://www.gpi-site.com ) for system requirements, downloads and installation instructions.

This code utilizes `cblas_dgemm` function, which is readily available in the MKL library. In case you use other BLAS / LAPACK library, make sure this function is available (e.g. CBLAS from the Netlib repository, http://www.netlib.org/blas ) and the cblas.h is included.

Compilation and running
------------

If using MKL:

```
source /path/to/mklvars.sh intel64
```

In distributed parallel runs, include this line in .bashrc file (or other automatically sourced file on shell instantiation).

Set the `GASPI` variable in Makefile to point to the root director of the GPI-2 installation and change other variables, if needed. Then `make` or `make clean`.

To run the program:

```
/path/to/gaspi_run -m NODEFILE matrix_multi_gaspi_one_block 1000
```

where "1000" is the size of multiplied (square) matrices and  "NODEFILE" is a text file with list of host names of parallel compute nodes (one item per line). If more ranks per node should be deployed, host name is repeated.
